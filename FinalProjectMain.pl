#!/usr/bin/perl
use strict;
use Switch;
use IO::Socket;
########################################################################################################
#	Author:		Kevin Agosto
#	Description:	This script performs basic reconnaissace on a target: Port Scan, Banner Grab,
#			Attempts to look for exploits using google, and performs multiple DNS queries.
#	Version:	1.0                                                                            
########################################################################################################
# 					Global variables are defined here
my $target;
my $parsedDomain;
my $IP;
my $date = `date +%m-%d-%y`;
chomp($date);
my $outputFile = "output-$date.html";
########################################################################################################
#					SUBROUTINES ARE DEFINED HERE

# Dumps contents to a file
sub dump_array_to_file {
	
	my @array = @{$_[0]};				# Contains the pointer to the array
	open(OUTPUTFILE, ">>$outputFile");

	print(OUTPUTFILE "<h1>$_[1]</h1>\n");

	# Save everything in our array to the file			
	foreach (@array) {
		print(OUTPUTFILE "$_\n<br>\n");
	}
	print(OUTPUTFILE "\n");
	close(OUTPUTFILE);
	return 0;
}

# HTTP Banner grab via Socket
sub banner_grab  {

	# Variables are defined here
	my $host = "$_[0]";				# Contains the target host or IP
	my $request;					# Request
	my $port = $_[1];				# Port
	my $proto = "tcp";				# Protocol TCP
	my $sock;					# Socket descriptor
	my @headers;					# Will contain all the headers
	my $server;					# Contains the server information
	
	# Setup the socket (file) descriptor
	$sock = IO::Socket::INET->new(PeerAddr => $host,
			              PeerPort => $port,
                                      Proto    => $proto,
				      Timeout  => 3) || die fatal_error("[!!] Could not create socket!!");

	# Retrieve headers if an HTTP port
	if($port == 80 || $port == 8080)   {
		
		# Perform the banner grab and save to array
		print("[*] HTTP Banner grabbing $host\n");
		$request = "HEAD / HTTP/1.0\r\n\r\n";
		$sock->print($request);
		while(<$sock>)
		{
			push(@headers, "$_");
		
		}

		# Close the socket cleanly
		close($sock);
		print("[*] HTTP Banner Grab complete!\n\n");
		
		# Dump to file
        	dump_array_to_file(\@headers, "HTTP Banner");

        	foreach (@headers) {
                	if($_ =~ m/Server/)   {
                        	$server = $_;
                        	$server =~ s/Server: //;
               		}
        	}
		
		# Check for exploit via google
		print("[*] Googling exploits for service: $server\n");
		system("./exploitSearch.pl '$outputFile' '$server'");

		return $server;
	}

	# Banner grab non HTTP ports
	print("[*] Banner Grabbing $host on port $port\n");
	$request = "123454";
	$sock->print($request);
	$sock->recv($server, 1024);
	
        # Close the socket cleanly
        close($sock);
	print("[*] Banner Grab complete!\n\n");

	# Check for exploits via google
	print("[*] Googling exploits for service: $server\n");
	system("./exploitSearch.pl '$outputFile' '$server'");

	return $server;	
}

# Simple Port Scanner
sub simplePortScanner {

	my $host = "$_[0]";
	my @ports = (20, 21, 22, 25, 69, 80, 110, 135, 139, 161, 445, 1337, 3389, 4444, 5900, 8080, 31337);
	my $proto = "tcp";
	my $sock;
	my @array;

	foreach (@ports) {
		# Set up socket file descriptor
		$sock = IO::Socket::INET->new(PeerAddr => $host,
                	                      PeerPort => $_,
                        	              Proto    => $proto,
					      Timeout  => 1);

		print ("[*] Scanning port $_: ");
		if( $sock )
		{
			print(" Open\n");
			if( $_ == 80 || $_ == 8080 )  {
				push(@array, "[*] FOUND PORT $_ OPEN: " . banner_grab($_[0], $_)); 
			}
			if( $_ == 21 || $_ == 22 )   {
				push(@array, "[*] FOUND PORT $_ OPEN: " . banner_grab($_[0], $_));
			}

			push(@array, "[*] FOUND PORT $_ OPEN");	
		}
		else
		{
			print(" Closed\n");
		}
	}
				

	dump_array_to_file(\@array, "Port Scan Results");

	return 0;
}

# Function for reporting errors, returns 1
sub fatal_error   {

	print("$_[0]");
	exit 1;

}

# The lame banner
sub lame_banner   {
	print("########################################################################\n");
	print("#			A Simple Target Recon Tool                    #\n");
	print("#			    by Kevin Agosto                           #\n");
	print("########################################################################\n");
}



#########################################################################################################
#					MAIN FUNCTION HERE

# Define variables here
my $OPTION;				# This variable contain the option the user inputs
my $OUTPUTFILE;

# Display the banner
lame_banner();

# Ask user for the target
print("[+] Enter the target's domain name or IP address: ");
$target = <STDIN>;
chomp($target);

# Create the output file, and add proper HTML tags
open($OUTPUTFILE, ">$outputFile") || die fatal_error("[!!] Could not open $outputFile\n");
print($OUTPUTFILE "<html>\n<center><h1>Report for $target</h1></center>\n");
close($OUTPUTFILE);

# Prompt the user to select an option
print("[+] What would you like to do?\n\t[1] HTTP Banner Grab\n\t[2] TCP connect Port Scan\n\t[3] DNS Reconnaissance\n\t[4] All (Recommended)\n\t[5] Quit\n");
print("[+] Enter a number: ");
$OPTION=<STDIN>;			# Accept option from standard input
chomp($OPTION);				# Strip off newlines

switch($OPTION) 
{
	case 1	
	{ 
		banner_grab( $target, 80 );
	}
	case 2
	{
		simplePortScanner($target);
	}
	case 3  
	{
		# If the target is a FQDN parse it
		if($target =~ m/^([a-zA-Z]*)\.([a-zA-Z]*)\.([a-zA-Z]*)$/ ) {
			$parsedDomain = $target;
			$parsedDomain =~ s/^([^\.]+)\.//;
			$IP = `host "$parsedDomain"`;
			$IP =~ s/$parsedDomain has address//;
			system("./dnsRecon.sh '$outputFile' '$parsedDomain' '$IP'");
		}
		# Else if the target is an IP
		elsif($target =~ m/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/) {
			system("./dnsRecon.sh '$outputFile' '0' '$target'");
		}
		# Else if the target does not have a subdomain 
		else   {
			$IP = `host "$target"`;
			$IP =~ s/$target has address//;
			system("./dnsRecon.sh '$outputFile' '$target' '$IP'");
		}
	}
	case 4
	{
		simplePortScanner($target);

		# If the target is a FQDN parse it
		if($target =~ m/^([a-zA-Z]*)\.([a-zA-Z]*)\.([a-zA-Z]*)$/ ) {
			$parsedDomain = $target;
			$parsedDomain =~ s/^([^\.]+)\.//;
			$IP = `host "$parsedDomain"`;
			$IP =~ s/$parsedDomain has address//;
			system("./dnsRecon.sh '$outputFile' '$parsedDomain' '$IP'");
		}
		# Else if the target is an IP
		elsif($target =~ m/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/) {
			system("./dnsRecon.sh '$outputFile' '0' '$target'");
		} 
		# Else if the target does not have a subdomain 
		else   {
			$IP = `host "$target"`;
			$IP =~ s/$target has address//;
			system("./dnsRecon.sh '$outputFile' '$target' '$IP'");
		}
 
	}
	case 5
	{
		print("[*] EXITING!!\n");
		exit 0;
	}
	else	
	{ 
		print("[!!] Please select a valid option\n\n");
	}
}

# Add the ending HTML tag and close
open($OUTPUTFILE, ">>$outputFile");
print($OUTPUTFILE "</html>\n");
close($OUTPUTFILE);

print("[-] Your report is at $outputFile, go ahead and open it with a browser ;-)\n");
# EOF
