					This is the README file for the final class project by Kevin Agosto:

1. Dependencies:

	Linux environment with Perl and Bash
	Perl modules: WWW::Mechanize and IO::Socket
	host and whois (both included in linux)

	./dnsRecon.sh 		-  DNS Recon tool (project 2.1 modified)
	./exploitSearch.sh	-  A script to fetch results from google 
	./hosts-list.txt	-  Dictionary file for the DNS Recon tool

2. Instructions:

	The main script is "./FinalProjectMain.pl", needless to say this is the script you must run. Although the scripts
	"./dnsRecon.sh" (used for the DNS reconnaissance) and "./exploitSearch.pl" (used to google up possible exploits) exist,
	it not advisable to run them each on their own (unless you know how to pass the right arguments.

3. TODO:

	These scripts have been written to meet a deadline, this means that much can be enhanced/re-written for cleaner purposes. 

	Things to fix: 
		Output can definitely be cleaner.
		Regex for cases 3 and 4 can be cleaner, and evaluation could be better. Further testing is needed.
		Fix the minor bug that outputs the port twice when banner grabbing is successfull.

	Things to add:
		IPv6 support
		Interaction with the Shodan API
		Search the CVE DB directly (because google has a lot of false positives)
		
4. BUGS:

	Minor:
	  The TCP connnect portscan has a problem with the output, after a successfull banner grab, it will dump to file that the port is open
	  twice.
