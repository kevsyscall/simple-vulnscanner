#!/bin/bash
############################################################################################################
#	Description:		This script two basic types of DNS reconnaissance: Brute force and Reverse.
#				Edited and meant to be used with the final project script.
#	Author:			Kevin Agosto
#	Version:		2.0
############################################################################################################

# Essential Files are defined here
OUTPUT_FILE="$1"      # Default output file
BRUTE_LIST="host-list.txt"			# Default dictionary of hosts, change accordingly

################################################################################################################################################
# 						FUNCTIONS ARE DEFINED IN THIS BLOCK

# Hosts Brute Force
function hostBrute   {
	# Verify if the dictionary exists
	if [ ! -f $BRUTE_LIST ]; then
		fatal_error "[!!] Fatal, could not find $BRUTE_LIST"
	fi

	# Start host brute procedure
	DOMAIN="$1"
	
	# Check if a domain was supplied
	if [ -z "$DOMAIN" ]; then
		fatal_error "[!!] Fatal, you must supply a domain name!"
	fi

	echo "[*] Starting Host Brute Forcer"
	echo -ne "\n<h1>Host Bruteforcer</h1>\n" >> $OUTPUT_FILE 
	# Iterate through the list	
	for HOSTS in $(cat "$BRUTE_LIST") ; do
		host "$HOSTS.$DOMAIN" | grep "has address" >> $OUTPUT_FILE && echo -ne "\n<br>\n" >> $OUTPUT_FILE
	done

	echo "[-] Host brute done! Contents dumped to $OUTPUT_FILE"		
}

# Reverse query
function reverseQuery   {
	# Set target's IP
	TARGET_IP="$1"
	
	# Check if an IP address was supplied
	if [ -z "$TARGET_IP" ]; then
		fatal_error "[!!] Fatal, no IP address was supplied"
	fi

	# Parse the TARGET_IP address for C class reverse query
	FIRST_OCTET=$(echo "$TARGET_IP" | cut -d"." -f1)
	SECOND_OCTET=$(echo "$TARGET_IP" | cut -d"." -f2)
	THIRD_OCTET=$(echo "$TARGET_IP" | cut -d"." -f3)
	PARSED_TARGET_IP="$FIRST_OCTET.$SECOND_OCTET.$THIRD_OCTET"


	# Whois to determine ip range	
	echo "[*] Determining target's IP range via whois"
	ADDRESS_RANGE=$(whois -b "$TARGET_IP" | grep "inetnum")
	echo "[*] Target owns $ADDRESS_RANGE"

	# Reverse query the whole C class of the target ip address
	echo "[*] Reverse querying $TARGET_IP/24"
	echo "[*] THIS FUNCTION DOES NOT QUERY THE WHOLE RANGE OWNED BY THE TARGET!"
	echo -ne "\n<h1> Reverse Query $TARGET_IP/24</h1>\n" >> $OUTPUT_FILE
	for i in $(seq 0 255); do
		host "$PARSED_TARGET_IP.$i" | grep "domain name pointer" >> $OUTPUT_FILE && echo -ne "<br>\n" >> $OUTPUT_FILE
	done

	echo "[-] Reverse query done! Contents dumped to $OUTPUT_FILE"
}

# Fatal Error
function fatal_error {
	echo "$1"
	exit 1
}

################################################################################################################################################
# 							       MAIN SCRIPT

hostBrute $2
reverseQuery $3

# Exit Cleanly
echo "[*] Exiting!"
exit 0

# EOF


